library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.VGApackage.all;

entity effectLogic is
	port 
	(
		aclrn, clk, run, vga_vs : in std_logic;
		xcol, yrow : in vga_xy;
		effect : in std_logic_vector(2 downto 0);
		eFunc, stop : out std_logic
	);

end entity;

architecture behavioral of effectLogic is
begin

	effects : process(aclrn, clk, run, vga_vs, xcol, yrow, effect)
	variable vga_last : std_logic := '1';
	variable run_last : std_logic := '0';
	variable x, y : integer;
	variable t : integer range 0 to 511:= 0;
	constant E1_t_max : integer := 114;
   constant E2_t_max : integer := 148;
	constant E3_t_max : integer := 57;
	constant E4_t_max : integer := 256;
	constant ELIPSE_CONST : integer := 57;
	
	begin
		x := to_integer(xcol);
		y := to_integer(yrow);
		
		if(aclrn = '0') then
			t := 0;
			vga_last := '1';
			run_last := '0';
		elsif(rising_edge(clk)) then
			if(vga_vs = '0' and vga_last = '1') then t := t + 1;
			end if;
			
			if(run = '1' and run_last = '0') then --New effect started
				t := 0;
			end if;
			
			if(((effect = "001") and (t > E1_t_max)) or
				((effect = "010") and (t > E2_t_max)) or
				((effect = "011") and (t > E2_t_max)) or
				((effect = "100") and (t > E3_t_max)) or
				((effect = "101") and (t > E4_t_max)) or
				((effect = "110") and (t > E4_t_max)) or
				 (effect = "000") or (run = '0')) then stop <= '1';
			else stop <= '0';
			end if;
			
			if(run = '0') then eFunc <= '1'; --Not running => stable eFunc;
			elsif(effect = "001") then --E1 ELIPSE
				if(((x - 320) * (x - 320) * 9 + (y - 240) * (y - 240) * 16) < (t * t * 9 * 16)) then eFunc <= '1';
				else eFunc <= '0';
				end if;
			elsif(effect = "010") then --E2f SLANT LINES FORWARD
				if(((3 * x - 13 * t) < (4 * y)) and ((3 * x  + 13 * t) > (4 * y))) then eFunc <= '1';
				else eFunc <= '0';
				end if;
			elsif(effect = "011") then --E2b SLANT LINES BACKWARD
				if(((3 * x - 13 * (E2_t_max - t)) < (4 * y)) and (((3 * x  + 13 * (E2_t_max - t)) > (4 * y)))) then eFunc <= '0';
				else eFunc <= '1';
				end if;
			elsif(effect = "100") then --E3 TWO ELIPSES
				--this if could be optimized
				if((((x - 320) * (x - 320) * 9 + (y - 240) * (y - 240) * 16) < ((ELIPSE_CONST + t) * (ELIPSE_CONST + t) * 9 * 16)) and
					(((x - 320) * (x - 320) * 9 + (y - 240) * (y - 240) * 16) > ((ELIPSE_CONST - t) * (ELIPSE_CONST - t) * 9 * 16))) then eFunc <= '1';
				else eFunc <= '0';
				end if;
			elsif(effect = "101") then --E4f SNAKE OVER SNAKE FORWARD
				if(((t / 32) * 60 > y) or                                                                               --/////////////
				 ((((t / 32) * 60 + 30) >= y) and (((t mod 32) * 20) > x)) or                                           --/////>.......
				(((((t / 32) + 1) * 60) >= y) and (((t / 32) * 60 + 30) < y) and (((t mod 32) * 20) > (640 - x)))) then --.......</////
				eFunc <= '1';
				else eFunc <= '0';
				end if;
			else --E4b SNAKE OVER SNAKE BACKWARD
				if((((E4_t_max - t) / 32) * 60 > y) or                                                     --/////////////
				 (((((E4_t_max - t) / 32) * 60 + 30) >= y) and ((((E4_t_max - t) mod 32) * 20) > x))  or   --//////////<..                                      				 
				((((((E4_t_max - t) / 32) + 1) * 60) >= y) and ((((E4_t_max - t) / 32) * 60 + 30) < y) and --..>//////////
				 (((((E4_t_max - t) mod 32)) * 20) > (640 - x)))) then eFunc <= '0';
				else eFunc <= '1';
				end if;
			end if;
			vga_last := vga_vs;
			run_last := run;
		end if;

	end process;

end architecture;
