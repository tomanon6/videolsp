-------------------------------------------------------------
-- CTU-FFE Prague, Dept. of Control Eng. [Richard Susta]
-- Published under GNU General Public License
-------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;       -- type integer and unsigned
use work.VGApackage.all;

entity drevoDL640 is 
port(	yrow, xcolumn : in vga_xy; -- row and  column indexes of VGA video
      VGA_CLK : in std_logic; -- pixel clock
	   VGA_RGB: out std_logic_vector(23 downto 0)	 );  --  color information
end;

architecture behavioral of drevoDL640 is
---------------------------------------------------------------------------------
-- Used colors 	
constant RED : RGB_type := ToRGB(196,0,0);
constant GREEN : RGB_type := ToRGB(0,96,0);
constant BROWN : RGB_type := ToRGB(176,91,52);
constant YELLOW : RGB_type := ToRGB(X"BFBF00"); -- or ToRGB(191,191,0); 
constant BLACK : RGB_type := ToRGB(X"000000");  -- or ToRGB(0,0,0);
---------------------------------------------------------------------------
constant MEMROWSIZE : integer := 256; -- memory organization
constant MEMROWCOUNT : integer := 256;
constant EMBORGX1 : integer := 40; -- positions of picture in the flag
constant EMBORGY1 : integer := 32;
constant EMBORGX2 : integer := XSCREEN-MEMROWSIZE-40; -- positions of picture in the flag
constant EMBORGY2 : integer := YSCREEN-MEMROWCOUNT-32;
constant MEM_END_ADDRESS : integer := 4095; -- not used here

component axeROM	port(	address : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
		clock   : IN  STD_LOGIC;
		q       : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
end component axeROM;

signal picture_address_s : std_logic_vector(15 DOWNTO 0); -- address sent to memory
signal picture_q_s : std_logic_vector(1 downto 0); -- obtained data
signal VGA_CLK_n:std_logic; -- negated VGA_CLK
begin
   VGA_CLK_n <= not VGA_CLK;

	rom_inst : axeROM
   PORT MAP(clock => VGA_CLK_n, address => picture_address_s, q => picture_q_s);
  
Bigflag : process(xcolumn, yrow, picture_q_s) -- output of process depends on xcolumn and yrow
    variable RGB : RGB_type; -- output colors
    variable x, y : integer; -- renamed xcolumn and yrow
	 variable romID:integer range 0 to 2;
    begin
		x:=to_integer(xcolumn); y:=to_integer(yrow); -- convert to integer      
		romID:=0;
		if(x>=EMBORGX1 and x<EMBORGX1+MEMROWSIZE 
			and y>=EMBORGY1 and y<EMBORGY1+MEMROWCOUNT) then romID:=1;
		end if;
		
		if(x>=EMBORGX2 and x<EMBORGX2+MEMROWSIZE 
			and y>=EMBORGY2 and y<EMBORGY2+MEMROWCOUNT) then romID:=2;
		end if;
		
		if(x<0) or (x>=XSCREEN) or (y<0) or (y>=YSCREEN) then  RGB:=BLACK; --black outside of visible frame 
		elsif romID>0 and picture_q_s /= "10" then -- no picture, background is transparent
			if romID=1 then
			  if picture_q_s = "00" then RGB:= BLACK; else RGB:= BROWN; end if; 
			else
				if picture_q_s = "00" then RGB:= BLACK; else RGB:= GREEN; end if;
			end if;
		elsif (x-160)*(x-160) + y*y < YSIZE*YSIZE then   RGB:=YELLOW;
		elsif (x-XSCREEN+160)*(x-XSCREEN+160) + (y-YSCREEN)*(y-YSCREEN) < YSIZE*YSIZE then   RGB:=YELLOW;
		elsif 2*y < 2*YSCREEN - 3*x then  RGB:=YELLOW;  -- line equation  y = 480-(3/2)*x
		elsif 2*y > -3*(x-XSCREEN) then    RGB:=YELLOW; -- line equation  y = -(3/2)*(x-640)
		elsif y<YSIZE then RGB:=GREEN;
		else  RGB:=BROWN;
		end if;
	
		case romID is
			when 1 =>
				 picture_address_s <= std_logic_vector(to_unsigned((y-EMBORGY1)*MEMROWSIZE + (x-EMBORGX1),
																									 picture_address_s'LENGTH));
			when 2 =>
			-- we rotate rom coordinates by 90 degrees clockwise by matrix [0 1; -1 0]*[x y], i.e, xrom=-y, yrom=x
				 picture_address_s <= std_logic_vector(
							to_unsigned((+EMBORGY2+MEMROWCOUNT-1-y)*MEMROWSIZE + (-x+EMBORGX2),--
							picture_address_s'LENGTH));
			  when others =>     picture_address_s <=(others=>'0'); 
		end case; 
		-- Copy results in RGB to outputs of entity
		VGA_RGB <= RGB.R & RGB.G & RGB.B;
-----------------------------------------------------------------------------
end process;

end architecture behavioral;
