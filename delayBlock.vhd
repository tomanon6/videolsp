library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity delayBlock is
	port 
	(
		aclrn, clk, vga_vs, delaySwitch, stop : in std_logic;
		nextState : out std_logic
	);

end entity;

architecture behavioral of delayBlock is
signal stop_thin : std_logic;
signal timer : std_logic;
begin
	process(aclrn, clk, vga_vs, delaySwitch, stop)
	variable vga_last : std_logic := '0';
	variable stop_last : std_logic := '0';
	variable counter : integer range 0 to 180 := 0;
	begin
		
		if(aclrn = '0') then
			counter := 0;
			vga_last := '0';
			stop_last := '0';
		elsif(rising_edge(clk)) then
			if(vga_vs = '0' and vga_last = '1' and stop = '1') then
				counter := counter + 1;
			elsif(stop = '0') then
				counter := 0;
			end if;

			if(stop = '1' and stop_last = '0') then
				stop_thin <= '1';
			else
				stop_thin <= '0';
			end if;

			if(delaySwitch = '0' and counter >= 60) then
				counter := 0;
				timer <= '1';
			elsif(counter >= 180) then
				counter := 0;
				timer <= '1';
			else
				timer <= '0';
			end if;

			vga_last := vga_vs;
			stop_last := stop;

		end if;
	end process;

	nextState <= stop_thin or timer;

end architecture;
