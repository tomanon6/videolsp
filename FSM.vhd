library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FSM is
	port 
	(
		aclrn, clk, nextState : in std_logic;
		run : out std_logic;
		effect : out std_logic_vector(2 downto 0);
		addr1, addr2 : out std_logic_vector(1 downto 0)
	);

end entity;

architecture behavioral of FSM is
begin
	process(aclrn, clk, nextState) is
	type state_type is (korunov640A, korunov640B, korunov320, drevo640A, drevo640B, drevo320, E1, E2f, E2b, E3, E4f, E4b);
	variable state : state_type := korunov640A;
	begin
		if(aclrn = '0') then 
			state := korunov640A;
		elsif(rising_edge(clk) and nextState = '1') then
			case state is
				when korunov640A => state := E1;
				when E1 =>          state := drevo640A;
				when drevo640A =>   state := E2f;
				when E2f =>         state := drevo320;
				when drevo320 =>    state := E2b;
				when E2b =>         state := drevo640B;
				when drevo640B =>   state := E3;
				when E3 =>          state := korunov640B;
				when korunov640B => state := E4f;
				when E4f =>         state := korunov320;
				when korunov320 =>  state := E4b;
				when E4b =>         state := korunov640A;
			end case;
		end if;

		case state is
			when korunov640A => run <= '0'; effect <= "000"; addr1 <= "00"; addr2 <= "00";
			when E1 =>          run <= '1'; effect <= "001"; addr1 <= "00"; addr2 <= "01";
			when drevo640A =>   run <= '0'; effect <= "000"; addr1 <= "01"; addr2 <= "01";
			when E2f =>         run <= '1'; effect <= "010"; addr1 <= "01"; addr2 <= "10";
			when drevo320 =>    run <= '0'; effect <= "000"; addr1 <= "10"; addr2 <= "10";
			when E2b =>         run <= '1'; effect <= "011"; addr1 <= "10"; addr2 <= "01";
			when drevo640B =>   run <= '0'; effect <= "000"; addr1 <= "01"; addr2 <= "01";
			when E3 =>          run <= '1'; effect <= "100"; addr1 <= "01"; addr2 <= "00";
			when korunov640B => run <= '0'; effect <= "000"; addr1 <= "00"; addr2 <= "00";
			when E4f =>         run <= '1'; effect <= "101"; addr1 <= "00"; addr2 <= "11";
			when korunov320 =>  run <= '0'; effect <= "000"; addr1 <= "11"; addr2 <= "11";
			when E4b =>         run <= '1'; effect <= "110"; addr1 <= "11"; addr2 <= "00";
		end case;

	end process;
end architecture;
