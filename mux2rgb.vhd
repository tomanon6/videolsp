library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2rgb is
	port 
	(
		RGB1, RGB2 : in std_logic_vector(23 downto 0);
		sel : in std_logic;
		RGB_OUT : out std_logic_vector(23 downto 0)
	);

end entity;

architecture dataflow of mux2rgb is
begin

	with sel select
		RGB_OUT <= RGB1 when '0',
			   RGB2 when others;

end dataflow;
