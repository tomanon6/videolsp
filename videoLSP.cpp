#include "SDL.h"
#include <iostream>

using namespace std;

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#define MAXCOUNT 256
#define STEP 1
#define ELIPSE_CONST 114

struct RGB
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

RGB black = {0, 0, 0};

void drawFrame(SDL_Renderer* renderer, int t, int mode);
RGB korunov320(int xcol, int yrow);
RGB korunov640(int xcol, int yrow);
RGB drevo320(int xcol, int yrow);
RGB drevo640(int xcol, int yrow);

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* window = SDL_CreateWindow("videoLSP", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Event inputEvent;
    bool run = true;

    int t = 0;
    int mode = 0;
    int maxt[] = {114, 148, 114, 256};


    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    drawFrame(renderer, t, mode);

    SDL_RenderPresent(renderer);

    while(run)
    {
        SDL_PollEvent(&inputEvent);

        if(inputEvent.type == SDL_QUIT || (inputEvent.type == SDL_KEYDOWN && inputEvent.key.keysym.sym == SDLK_ESCAPE))
        {
            run = false;
        }

        if(inputEvent.type == SDL_KEYDOWN && inputEvent.key.keysym.sym == SDLK_SPACE)
        {
            t = (t + STEP);
            if(t == maxt[mode])
            {
                int stop = 1;//no purpose here
                cout << "stop = " << stop << endl;
            }else if(t > maxt[mode])
            {
                t = 0;
                mode = (mode + 1) % 4;
            }

            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderClear(renderer);

            drawFrame(renderer, t, mode);

            SDL_RenderPresent(renderer);
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void drawFrame(SDL_Renderer* renderer, int t, int mode)
{
    cout << "t = " << t << endl;

    RGB color = black;

    for(int yrow = 0; yrow < 480; yrow++)
    {
        for(int xcol = 0; xcol < 640; xcol++)
        {
            

            //A ELIPSE
            if (mode == 0)
            {
                if(((xcol - 320) * (xcol - 320) * 9 + (yrow - 240) * (yrow - 240) * 16) < (t * t * 9 * 16))
                {
                    
                    color = korunov640(xcol, yrow);
                }
                else
                {
                    color = drevo640(xcol, yrow);
                }
            }
            //B SLANT LINES
            else if (mode == 1)
            {
                if((3 * xcol - 13 * t < 4 * yrow) && (3 * xcol  + 13 * t > 4 * yrow))
                {
                    color = korunov640(xcol, yrow);
                }
                else
                {
                    color = drevo640(xcol, yrow);
                }
            }
            //C TWO ELIPSES
            else if (mode == 2)
            {
                //this if could be optimized
                if(((xcol - 320) * (xcol - 320) * 9 + (yrow - 240) * (yrow - 240) * 16 < (ELIPSE_CONST + t >> 1) * (ELIPSE_CONST + t >> 1) * 9 * 16) &&
                   ((xcol - 320) * (xcol - 320) * 9 + (yrow - 240) * (yrow - 240) * 16 > (ELIPSE_CONST - t >> 1) * (ELIPSE_CONST - t >> 1) * 9 * 16))
                {
                    
                    color = korunov640(xcol, yrow);
                }
                else
                {
                    color = drevo640(xcol, yrow);
                }
            }
            //D SNAKE OVER SNAKE
            else
            {
                if((t >> 5) * 60 > yrow ||                                                                              /*/////////////*/
                ((((t >> 5) * 60 + 30) >= yrow) && (((t % 32)) * 20 > xcol))  ||                                        /*/////>.......*/
                (((((t >> 5) + 1) * 60) >= yrow) && (((t >> 5) * 60 + 30) < yrow) && (((t % 32)) * 20 > - xcol + 640))) /*.......</////*/
                {
                    color = korunov640(xcol, yrow);
                }
                else
                {
                    color = drevo640(xcol, yrow);
                }
            }

            SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 255);
            SDL_RenderDrawPoint(renderer, xcol, yrow);
        }
    }
}

RGB korunov320(int xcol, int yrow)
{
    RGB color = black;

    RGB red = {218, 37, 29};
    RGB blue = {0, 124, 195};
    RGB violet = {151, 69, 120};

    int x = xcol - 160;
    int y = yrow - 120;

    if((x - 112) * (x - 112) + (y - 120) * (y - 120) <= 64 * 64 && y <= 120)
    {
        color = violet;
    }
    else if((x - 208) * (x - 208) + (y - 120) * (y - 120) <= 64 * 64 && y >= 120)
    {
        color = violet;
    }
    else if(4 * y <= 960 - 3 * x && x > 0 && y > 0)
    {
        color = red;
    }
    else if(4 * y > 960 - 3 * x && x < 320 && y < 240)
    {
        color = blue;
    }
    else
    {
        color = black;
    }

    return color;
}

RGB korunov640(int xcol, int yrow)
{
    RGB color = black;

    RGB red = {218, 37, 29};
    RGB blue = {0, 124, 195};
    RGB violet = {151, 69, 120};

    int x = xcol;
    int y = yrow;

    if((x - 224) * (x - 224) + (y - 240) * (y - 240) <= 128 * 128 && y <= 240)
    {
        color = violet;
    }
    else if((x - 416) * (x - 416) + (y - 240) * (y - 240) <= 128 * 128 && y >= 240)
    {
        color = violet;
    }
    else if(4 * y <= 1920 - 3 * x)
    {
        color = red;
    }
    else if(4 * y > 1920 - 3 * x && x < 640 && y < 480)
    {
        color = blue;
    }
    else
    {
        color = black;
    }

    return color;
}

RGB drevo320(int xcol, int yrow)
{
    RGB color;

    RGB yellow = {191, 191, 0};
    RGB green = {0, 96, 0};
    RGB brown = {176, 91, 52};

    int x = xcol - 160;
    int y = yrow - 120;

    if((x < 0) || (x >= 320) || (y < 0) || (y >= 240))
    {
        color = black;
    }
    else if((x - 80) * (x - 80) + y * y < 240 * 240 / 4)
    {
        color = yellow;
    }
    else if((x - 320 + 80) * (x - 320 + 80) + (y - 240) * (y - 240) < 240 * 240 / 4)
    {
        color = yellow;
    }
    else if(2 * y < 2 * 240 - 3 * x)
    {
        color = yellow;
    }
    else if(2 * y > -3 * (x - 320))
    {
        color = yellow;
    }
    else if(y < 240 / 2)
    {
        color = green;
    }
    else
    {
        color = brown;
    }

    return color;
}

RGB drevo640(int xcol, int yrow)
{
    RGB color;

    RGB yellow = {191, 191, 0};
    RGB green = {0, 96, 0};
    RGB brown = {176, 91, 52};

    int x = xcol;
    int y = yrow;

    if((x < 0) || (x >= 640) || (y < 0) || (y >= 480))
    {
        color = black;
    }
    else if((x - 160) * (x - 160) + y * y < 480 * 480 / 4)
    {
        color = yellow;
    }
    else if((x - 640 + 160) * (x - 640 + 160) + (y - 480) * (y - 480) < 480 * 480 / 4)
    {
        color = yellow;
    }
    else if(2 * y < 2 * 480 - 3 * x)
    {
        color = yellow;
    }
    else if(2 * y > -3 * (x - 640))
    {
        color = yellow;
    }
    else if(y < 480 / 2)
    {
        color = green;
    }
    else
    {
        color = brown;
    }

    return color;
}
