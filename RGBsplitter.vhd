library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RGBsplitter is
	port 
	(
		RGB_IN : in std_logic_vector(23 downto 0);
		R : out std_logic_vector(7 downto 0);
		G : out std_logic_vector(7 downto 0);
		B : out std_logic_vector(7 downto 0)
	);

end entity;

architecture dataflow of RGBsplitter is
begin

	R <= RGB_IN(23 downto 16);
	G <= RGB_IN(15 downto 8);
	B <= RGB_IN(7 downto 0);

end dataflow;
