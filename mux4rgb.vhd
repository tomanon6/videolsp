library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux4rgb is
	port 
	(
		RGB1, RGB2, RGB3, RGB4 : in std_logic_vector(23 downto 0);
		sel : in std_logic_vector(1 downto 0);
		RGB_OUT : out std_logic_vector(23 downto 0)
	);

end entity;

architecture dataflow of mux4rgb is
begin

	with sel select
		RGB_OUT <= RGB1 when "00",
			         RGB2 when "01",
			         RGB3 when "10",
			         RGB4 when others;

end dataflow;
