library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.VGApackage.all;

entity korunovDL320 is
	port 
	(
		xcolumn, yrow : in vga_xy;
		VGA_CLK : in std_logic;
		VGA_RGB: out std_logic_vector(23 downto 0)
	);
end entity;

architecture behavioral of korunovDL320 is 

constant BLACK : RGB_type := ToRGB(0, 0, 0);
constant RED : RGB_type := ToRGB(218, 37, 29);
constant BLUE : RGB_type := ToRGB(0, 124, 195);
constant VIOLET : RGB_type := ToRGB(151, 69, 120);
constant YELLOW : RGB_type := ToRGB(255, 245, 0);

constant MEMROWSIZE : integer := 64;
constant MEMROWCOUNT : integer := 32;
constant EMBORGX1 : integer := 80;
constant EMBORGY1 : integer := 72;
constant EMBORGX2: integer := XSIZE - MEMROWSIZE - EMBORGX1;
constant EMBORGY2 : integer := YSIZE - MEMROWCOUNT - EMBORGY1;
constant MEM_END_ADDRESS : integer := 2047;

component koruna320rom is
	port
	(
		address : in std_logic_vector(10 downto 0);
		clock : in std_logic := '1';
		q : out std_logic_vector(1 downto 0)
	);
end component;

signal picture_address_s : std_logic_vector(10 DOWNTO 0); -- address sent to memory
signal picture_q_s : std_logic_vector(1 downto 0); -- obtained data
signal VGA_CLK_n:std_logic; -- negated VGA_CLK

begin

	VGA_CLK_n <= not VGA_CLK;

	rom_inst : koruna320rom
   PORT MAP(clock => VGA_CLK_n, address => picture_address_s, q => picture_q_s);

	LSPflag : process(xcolumn, yrow, picture_q_s)
	variable RGB : RGB_type;
	variable x, y : integer;

	begin
		x := to_integer(xcolumn) - 160;
		y := to_integer(yrow) - 120;
		picture_address_s <= (others=>'0');
		
		if(EMBORGX1 < x and x < EMBORGX1 + MEMROWSIZE and EMBORGY1 < y and y < EMBORGY1 + MEMROWCOUNT) then	-- horni koruna
			picture_address_s <= std_logic_vector(to_unsigned((y - EMBORGY1) * MEMROWSIZE + (x - EMBORGX1), picture_address_s'LENGTH));
			
			case picture_q_s is
				when "00" => RGB := VIOLET;
				when "01" => RGB := BLUE;
				when "10" => RGB := YELLOW;
				when others => RGB := BLACK;
			end case;
			
		elsif(EMBORGX2 < x and x < EMBORGX2 + MEMROWSIZE and EMBORGY2 < y and y < EMBORGY2 + MEMROWCOUNT) then	-- dolni koruna
			picture_address_s <= std_logic_vector(to_unsigned((MEMROWCOUNT-1-(y-EMBORGY2)) * MEMROWSIZE + (x - EMBORGX2), picture_address_s'LENGTH));
			
			case picture_q_s is
				when "00" => RGB := VIOLET;
				when "01" => RGB := RED;
				when "10" => RGB := YELLOW;
				when others => RGB := BLACK;
			end case;
			
		elsif((x - 112) * (x - 112) + (y - 120) * (y - 120) <= 64 * 64 and y <= 120) then	-- horni pulkruh
			RGB := VIOLET;
		elsif((x - 208) * (x - 208) + (y - 120) * (y - 120) <= 64 * 64 and y >= 120) then	-- dolni pulkruh
			RGB := VIOLET;
		elsif(4 * y <= 960 - 3 * x and x > 0 and y > 0) then	-- levy horni
			RGB := RED;
		elsif(4 * y > 960 - 3 * x and x < 320 and y < 240) then	--pravy dolni
			RGB := BLUE;
		else	-- mimo obrazek
			RGB := BLACK;
		end if;

		VGA_RGB <= RGB.R & RGB.G & RGB.B;

		RGB := BLACK;

	end process;

end architecture;
